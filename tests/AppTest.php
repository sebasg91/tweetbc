<?php

use App\Posts\PostAggregator;

class AppTest extends TestCase
{

    public function testRootPageContent()
    {
        $this->get('/');

        $this->assertEquals(
            'Try /hello/:name', $this->response->getContent()
        );
    }

    public function testGreetingsPage()
    {
        $this->get('/hello/Sebastian');

        $this->assertEquals(
            'Hello Sebastian', $this->response->getContent()
        );
    }

    public function testItCountsHourlyPosts()
    {
        $mockPosts = Mockery::mock(PostAggregator::class);

        $mockPosts->shouldReceive('countHourlyPosts')
                ->andReturn($this->hourlyPostsCount());

        $this->app->instance(PostAggregator::class, $mockPosts);
       
        $this->json('GET', '/histogram/name')
            ->seeJsonEquals($this->hourlyPostsCount());
    }


    /**
     * Mocked Array for testing 
     *
     * @return Array
     */
    protected function hourlyPostsCount()
    {
        return Array (
            "00" => 1,
            "01" => 1,
            "02" => 2,
            "03" => 3,
            "04" => 4,
            "05" => 5,
            "06" => 6,
            "07" => 7,
            "08" => 8,
            "09" => 9,
            "10" => 1,
            "11" => 1,
            "12" => 1,
            "13" => 1,
            "14" => 2,
            "15" => 3,
            "16" => 4,
            "17" => 5,
            "18" => 6,
            "19" => 7,
            "20" => 8,
            "21" => 9,
            "22" => 1,
            "23" => 1
        );
    }
}
