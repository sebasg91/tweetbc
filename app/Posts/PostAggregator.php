<?php

namespace App\Posts;

interface PostAggregator
{

    public function countHourlyPosts($name);

}