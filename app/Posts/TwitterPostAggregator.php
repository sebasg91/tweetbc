<?php

namespace App\Posts;

use Carbon\Carbon;
use App\Posts\PostAggregator;
use App\Repositories\TwitterRepository;

class TwitterPostAggregator implements PostAggregator
{


    /**
     * @var TwitterRepository
     */
    protected $repo;


    /**
     * @param TwitterRepository $repo
     * @return void
     */
    public function __construct(TwitterRepository $repo){

        $this->repo = $repo;
    
    }


    /**
     * It counts the number of tweets for a particular user each differnt hour of the day
     *
     * @param string $name
     * @return Array
     */

    public function countHourlyPosts($name)
    {
        $posts = $this->repo->getStatuses($name);

        $hourKeys = array("0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23");

        $tweetsPerHour = array_fill_keys($hourKeys, 0);


        $now = Carbon::now();

        foreach($posts as $post) {

            $tweetTime = Carbon::parse($post->created_at);

            $hour = $tweetTime->hour;

            // Tweet is taken into account only if it happened in the last 24hs.
            if($now->diffInHours($tweetTime) < 24) {
                $tweetsPerHour[ $hour ] = $tweetsPerHour[ $hour ] + 1;
            }


        }


        return $tweetsPerHour;
    }

}