<?php

namespace App\Repositories;

use Abraham\TwitterOAuth\TwitterOAuth;

class TwitterRepository
{

    /**
     * Twitter Account key
     *
     * @var string
     */
    protected $consumerKey;

    /**
     * Twitter Account Secret
     *
     * @var string
     */
    protected $consumerSecret;

    /**
     * Twitter application generated token
     *
     * @var string
     */
    protected $accessToken;

    /**
     * Twitter application generated token password
     *
     * @var string
     */
    protected $accessTokenSecret;

    public function __construct() 
    {
        $this->consumerKey = getenv('CONSUMER_KEY'); 
        $this->consumerSecret = getenv('CONSUMER_SECRET'); 
        $this->accessToken = getenv('ACCESS_TOKEN');
        $this->accessTokenSecret = getenv('ACCESS_TOKEN_SECRET');
    }

    /**
     * Send credentials over to the Twitter API to create the connection 
     *
     * @return void
     */
    public function createTwitterConnection()
    {
        return new TwitterOAuth(
            $this->consumerKey, 
            $this->consumerSecret, 
            $this->accessToken, 
            $this->accessTokenSecret
        );
    }

    /**
     * Get the latest posts for a Twitter account
     *
     * @param string $name
     * @return void
     */
    public function getStatuses($name)
    {
        return $this->createTwitterConnection()
            ->get("statuses/user_timeline", ["screen_name" => $name, "exclude_replies" => true]); 
    }

}