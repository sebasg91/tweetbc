<?php 

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Posts\PostAggregator;
use App\Repositories\TwitterRepository;

class TwitterHandlerController extends Controller
{

    /**
     *
     * @param  PostAggregator $posts
     * @param  string $name
     * @return json string
     */
    public function countHourlyPosts(PostAggregator $posts, $name)
    {
        return response()->json( $posts->countHourlyPosts($name) );
    }


}