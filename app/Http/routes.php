<?php


use Symfony\Component\HttpFoundation\Response;



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function() { 
    return new Response('Try /hello/:name', 200);
});

$app->get('/hello/{name}', function ($name) {
    return new Response('Hello '.$name, 200);
});

$app->get('/histogram/{name}', 'TwitterHandlerController@countHourlyPosts');