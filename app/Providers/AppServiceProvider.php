<?php

namespace App\Providers;

use App\Posts\TwitterPostAggregator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\App\Posts\PostAggregator::class, TwitterPostAggregator::class);
    }
}
