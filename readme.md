# Big Commerce - Twitter API utilisation


## Description
Originally I started developing the app with Silex. Since I was not familiar with the framework I spent a considerable amount of time understanding its core and provided functionality out of the box. Not being happy with the way it manages (at least as I was implementing it) dependency injection and IoC, I decided to migrate the application to Lumen (Laravel's micro-framework).


## Installation

#### Run:  
> git clone https://sebasg91@bitbucket.org/sebasg91/tweetbc.git  
> cd tweetbc  
> composer self-update  
> composer install  

#### Configuration:  
A .env file must be created with the App Key (for encryption purposes) and provide your own twitter credentials / tokens  
A .env.example can be used as template. Please rename it to .env and update the mentioned variables  

#### Webserver:  
A rewrite function should be implemented to redirect every request to /public/index.php  


## Testing
Basic integration tests are provided

#### Run:
> phpunit

## Author
Sebastian Perez ([sgperez@gmail.com](mailto:sgperez@gmail.com))


## License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)